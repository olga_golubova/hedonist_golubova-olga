const loginAction = require('./LoginSettings/actions/LoginSettings_pa');
const pageAction = new loginAction();
const credentials = require('./testDate.json')
const common = require('./commonFunc')
const assert = require('assert');

describe('login page', () => {
    beforeEach(function(){
        console.log("block --before");
        browser.url(credentials.appUrllogin);
    });

    afterEach(function(){
        console.log("block --after");
        common.waitForNotification();
        browser.pause(5000);
        //browser.reloadSession();
    });

    it ('Incorrect credential login',() => {
        pageAction.makeLogin(credentials.incorrecEmail,credentials.incorrectPassword);
        common.getTestResult(credentials.incorrectCredentialsErroText);
        common.checkLinkChange(browser.getUrl(),credentials.loginUrl);
    });
});