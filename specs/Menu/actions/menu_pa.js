const menDropDown = require('../page/menu_po');
const menu = new menDropDown();

class MenuDropDownActions
{
    
    logOutOption()
    {
        menu.menuProfileDropDown.waitForDisplayed();
        menu.menuProfileDropDown.moveTo(); 
        menu.logOutOption.waitForDisplayed();
        menu.logOutOption.moveTo(); 
        menu.logOutOption.click();

    }

    myListOption()
    {
        menu.menuProfileDropDown.waitForDisplayed();
        menu.menuProfileDropDown.moveTo(); 
        menu.mylistOption.waitForDisplayed();
        menu.mylistOption.moveTo(); 
        menu.mylistOption.click();
    }

    signInMenu()
    {   menu.signInMenu.waitForDisplayed();
        menu.signInMenu.moveTo(); 
        menu.signInMenu.click();}
}


module.exports = MenuDropDownActions;