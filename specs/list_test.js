const menuAction = require('./Menu/actions/menu_pa');
const pageMenuAction = new menuAction();

const listAction = require('./List/actions/myList_pa');
const pageListAction = new listAction();

const addlistAction = require('./List/actions/addList_pa');
const pageAddListAction = new addlistAction();

const loginAction = require('./LoginSettings/actions/LoginSettings_pa');
const pageloginAction = new loginAction();

const credentials = require('./testDate.json')
const common = require('./commonFunc')

const assert = require('assert');


describe('list page', () => {

    beforeEach(function(){
        console.log("block --before");
        browser.url(credentials.appUrllogin);
        pageloginAction.makeLogin(credentials.email, credentials.password);
        pageMenuAction.myListOption();
    });

    afterEach(function(){
        console.log("block --after");
        //common.waitForNotification();
        browser.pause(5000);
        browser.reloadSession();
    });

    it ('Add list',() => {
        pageListAction.addNewList();
        pageAddListAction.saveNewList(credentials.listName);
        common.getTestResult(credentials.listSaveMsg);
    });

    it ('Delete list',() => {
        pageListAction.deleteFisrtList();
        pageListAction.confirmDelete();
        common.getTestResult(credentials.listDeleteMsg);
    });

});