const assert = require('assert');

class commonFunc
{

    waitForNotification(){
    const notification = $('div.toast div');
    notification.waitForDisplayed(10000, true);
    } 

    getTestResult(msgText)
    {
    const notification = $('div.toast div');
    return assert.strictEqual(notification.getText(), msgText);
    }

    checkLinkChange(linkUrl, currentLinkUrl)
    {
        return assert.strictEqual(linkUrl, currentLinkUrl);
    }
}

module.exports = new commonFunc();