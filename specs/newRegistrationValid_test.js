const registrationAction = require('./RegistrationSettings/actions/RegistrationSettings_pa');
const loginAction = require('./LoginSettings/actions/LoginSettings_pa');

const pageloginAction = new loginAction();
const pageAction = new registrationAction();

const menuAction = require('./Menu/actions/menu_pa');
const pageMenuAction = new menuAction();

const credentials = require('./testDate.json')

const common = require('./commonFunc')
const assert = require('assert');

const {URL} = require('url');

function getCurrentUrl(){
    const url = new URL(browser.getUrl());
    return url.hostname.toString() + url.pathname.toString();
}

describe('registration page', () => {
  
    beforeEach(function(){
        console.log("block --before");
        browser.url(credentials.appUrllogin);
    });


    afterEach(function(){
        console.log("block --after");
        //common.waitForNotification()        
        browser.pause(5000);
        browser.reloadSession();
    });

    it ('Correct registration',() => {
        pageMenuAction.signInMenu();
        pageAction.makeRegistration(credentials.firstName,credentials.lastName, credentials.emailRegistration, credentials.newPasswordRegistration);
        common.getTestResult(credentials.newRegistrationCreateText);
    });

    it ('Correct login',() => {        
        pageloginAction.makeLogin(credentials.emailRegistration,credentials.newPasswordRegistration);
        common.getTestResult(credentials.welcomLogin);
        common.checkLinkChange(getCurrentUrl(),credentials.searchUrl);
    });

});