class LoginPage{
    get emailInput() {return $('input[name=email]')};
    get newPasswordInput() {return $('input[type=password]')};
    get createButton() {return $('button.is-primary')};
	get notification() {$('div.toast div')};
};

module.exports = LoginPage