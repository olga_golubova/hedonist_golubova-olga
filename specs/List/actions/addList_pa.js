const addListPage = require('../pages/addList_po');
const page = new addListPage();

class addListPageActions
{
    
    saveNewList(listName)
    {
        page.listNameInput.waitForDisplayed(2000);
        page.listNameInput.clearValue();
        page.listNameInput.setValue(listName);

        page.searchPlaceInput.waitForDisplayed(2000);
        page.searchPlaceInput.click();

        page.searchPlace.waitForDisplayed(2000);
        page.searchPlace.click();

        page.saveListButton.waitForDisplayed(2000);
        page.saveListButton.click();
    }
 
}

module.exports = addListPageActions;
