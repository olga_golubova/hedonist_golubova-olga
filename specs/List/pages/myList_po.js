class mylistPage{
    get addNewListButton() {return $('a[href="/my-lists/add"]')};
    get deleteListButton() {return $('li:first-child button.button.is-danger')};

    get modalWindow() {return $('div.modal')};
    get modalDeleteButton() {return $('.animation-content.modal-content button.button.is-danger')};
};

module.exports = mylistPage