class addlistPage{

    get listNameInput() {return $('input#list-name')};
    get saveListButton() {return $('button.button.is-success')};
    get loadImageButton() {return $('div.file')};
    get searchPlaceInput() {return $('input.search-field')};
    get searchPlace() {return $('div.search-places__list ul li:first-child')};

};

module.exports = addlistPage;