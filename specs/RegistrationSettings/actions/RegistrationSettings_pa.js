const RegistrationPage = require('../page/RegistrationSettings_po');
const page = new RegistrationPage();

class RegistrationActions
{
    
    makeRegistration(name, lastname, email, password)
    {

        page.firstNameInput.waitForDisplayed(2000);
        page.firstNameInput.clearValue();
        page.firstNameInput.setValue(name);

        page.lastNameInput.waitForDisplayed(2000);
        page.lastNameInput.clearValue();
        page.lastNameInput.setValue(lastname);
 
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.clearValue();
        page.emailInput.setValue(email);

        page.newPasswordInput.waitForDisplayed(2000);
        page.newPasswordInput.setValue(password);

        page.createButton.waitForDisplayed(2000);
        page.createButton.click();
    }

 
}


module.exports = RegistrationActions;