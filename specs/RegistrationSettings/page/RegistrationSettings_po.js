class RegistrationPage{
    get firstNameInput() {return $('input[name=firstName]')};
    get lastNameInput() {return $('input[name=lastName]')};
    get emailInput() {return $('input[name=email]')};
    get newPasswordInput() {return $('input[type=password]')};
    get createButton() {return $('button.is-primary')};
};

module.exports = RegistrationPage; 